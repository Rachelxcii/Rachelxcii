![Ilustración_sin_título 15](https://user-images.githubusercontent.com/49941851/149986377-850cbf3f-050d-45e3-a6cf-441bbf519169.png)

<h1 align="center">Social Networks</h1>
<p align="center">
    <a href="https://www.linkedin.com/in/raquelrr/"><img src="https://user-images.githubusercontent.com/49941851/150092573-79fde916-7a18-4bfb-8b3e-c3b4fd0ea0e4.png" alt="Ilustración_sin_título 16" width="90" align="center">
      <a href="https://gitlab.com/Rachelxcii/Rachelxcii/-/wikis/home"><img src="https://user-images.githubusercontent.com/49941851/150093504-8c3ca668-f47b-4280-9104-f8a0e905c75c.png" alt="Ilustración_sin_título 20" width="90" align="center">
      <a href="https://app.codesignal.com/profile/rachelxcii"><img src="https://user-images.githubusercontent.com/49941851/150093771-ceb97df8-cf53-4d7b-aa16-5a732a512805.png" alt="Ilustración_sin_título 22" width="90" align="center">
      <a href="https://leetcode.com/Rachelxcii/"><img src="https://user-images.githubusercontent.com/49941851/150093365-f8d070ea-88c0-4f62-a638-2e657078352a.png" alt="Ilustración_sin_título 18" width="90" align="center">
      <a href="https://www.hackerrank.com/rachelxcii"><img src="https://user-images.githubusercontent.com/49941851/150093077-dd796a9b-6511-4718-968a-04755eb9c7f5.png" alt="Ilustración_sin_título 19" width="90" align="center">   
    </a>
</p>

---
# Tasks to improve skills:
          
   - [x] [Google IT Automation with Python](https://www.coursera.org/professional-certificates/google-it-automation), specialization program of Google.
   - [ ] Course [Machine Learning](https://www.coursera.org/learn/machine-learning), Stanford University.
   - [ ] Do testing environment for each code upload in my github.
   - [ ] Developing an algorithm for active galaxies classification.
 
---
# Short story about me

### I'm a Naval Engineer from Polytechnic University of Madrid, I also studied a Master in Astronomy and Astrophysics, while studying both masters and working as an intern, I started coding and, in that moment, I discovered the love of my life.

### I started with Matlab and Arduino, and later I coded using Python. I decided to apply my coding knowledges to my final project of Master in Astronomy and Astrophysics, its title was "Machine Learning Techniques for Classification of Active Galaxies".

### After finishing both careers, I've redirected all my efforts to improve my coding skills and knowledges. I hope one day to work on it as a job. Until then I will include some of my projects in this github.

---

![Ilustración_sin_título_24grwerg](https://gitlab.com/Rachelxcii/Rachelxcii/uploads/d556cf333b9c0966dbac808e1d194536/Ilustración_sin_t%C3%ADtulo_24grwerg.png)
